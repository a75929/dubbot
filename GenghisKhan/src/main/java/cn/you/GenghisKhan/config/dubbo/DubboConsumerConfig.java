package cn.you.GenghisKhan.config.dubbo;

import cn.you.GenghisKhan.common.extension.ExtensionLoader;
import cn.you.GenghisKhan.config.ConfigCentre;
import cn.you.GenghisKhan.config.bean.AppBean;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.MonitorConfig;
import org.springframework.context.annotation.Bean;

public class DubboConsumerConfig extends DubboConfig{
    @Bean
    public ConsumerConfig consumer()
    {
        AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
        ConsumerConfig consumerConfig=new ConsumerConfig();
        consumerConfig.setTimeout(appbean.getRpcTimeout());
        return consumerConfig;
    }

}
