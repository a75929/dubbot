package cn.you.GenghisKhan.common.excel;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Column {
    boolean required () default false;
    String code ();
    boolean primarykey () default false;
    Regular[] regular () default {};
    Regular[] noRegular () default {};
}