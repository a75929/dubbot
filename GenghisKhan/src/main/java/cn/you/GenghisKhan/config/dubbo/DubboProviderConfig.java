package cn.you.GenghisKhan.config.dubbo;

import cn.you.GenghisKhan.common.extension.ExtensionLoader;
import cn.you.GenghisKhan.common.utils.IpUtil;
import cn.you.GenghisKhan.config.ConfigCentre;
import cn.you.GenghisKhan.config.bean.AppBean;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.spring.AnnotationBean;
import org.springframework.context.annotation.Bean;

public class DubboProviderConfig extends DubboConfig {
    @Bean
    public ProtocolConfig protocol () {
        AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
        ProtocolConfig protocolConfig = new ProtocolConfig();
        protocolConfig.setPort(appbean.getRpcPort());
        protocolConfig.setName(appbean.getRpcProtocol());
        return protocolConfig;
    }

    @Bean
    public ProviderConfig provider () {
        AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setHost(IpUtil.getLocalIP());
        providerConfig.setValidation("true");
        providerConfig.setFilter("echo,classloader,RewriteGenericFilter,validation,context,trace,timeout,monitor,exception,-default");
        providerConfig.setRetries(0);
        providerConfig.setTimeout(appbean.getRpcTimeout());
        return providerConfig;
    }

    @Bean
    public AnnotationBean annotationBean () {
        AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
        AnnotationBean annotationBean = new AnnotationBean();
        annotationBean.setPackage(appbean.getRpcScan());
        return annotationBean;
    }

}
