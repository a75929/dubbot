package cn.you.GenghisKhan.common.dataset;

public class DataRow
{
    DataTable table;
    int index;
    public DataRow(DataTable table, int index)
    {
        this.table = table;
        this.index = index;
    }
    public Object getObject(int i) throws Exception
    {
    	Object a=null;
    	DataColumn column = this.table.Columns.getColumn(i);
    	return column.getObject(index);
    }	     
    public Object SetObject(int i,Object value) throws Exception
    {
    	DataColumn column = this.table.Columns.getColumn(i);
    	return column.setObject(index, value);
    }
    public Object getObject(String columnname) throws Exception
    {
    	Object a=null;
    	DataColumn column = this.table.Columns.getColumn(columnname);
    	if(column != null){
	    	if(column.type==String.class&&column.getObject(index)==null)
	    	{
	    		return "";
	    	}
    	}else{
    		return "";
    	}
    	return column.getObject(index);
    }	     
    public Object SetObject(String columnname,Object value) throws Exception
    {
    	DataColumn column = this.table.Columns.getColumn(columnname);
    	return column.setObject(index, value);
    }
}