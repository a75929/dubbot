package cn.you.GenghisKhan.common.dataset;

import java.util.ArrayList;



public final class DataRowCollection extends   ArrayList
{
	private DataTable table;

    public DataRowCollection(DataTable table)
    {
        this.table = table;
    }
    public DataRow Add(Object[] values) throws Exception
    {
        int count = table.Columns.size();
        if (count < values.length)
        {
            throw new Exception("输入数组长度大于此表中的列数。");
        }
        for (int i = 0; i < values.length; i++)
        {
            if (values[i] != null)
            {
                this.table.Columns.getColumn(i).setObject(this.size(), values[i]);
            }
            else
            {
                this.table.Columns.getColumn(i).setObject(this.size(), null);
            }
        }
        for (int j = values.length; j < count; j++)
        {
        	this.table.Columns.getColumn(j).setObject(this.size(), null);
        }
        DataRow row = new DataRow(table, this.size());
        super.add(row);
        return row;
    }
    public DataRow Add(DataRow coprow) throws Exception
    {
    	 int count = table.Columns.size();
         if (count < coprow.table.Columns.size())
         {
             throw new Exception("输入数组长度大于此表中的列数。");
         }
         for (int i = 0; i < coprow.table.Columns.size(); i++)
         {
        	 DataColumn column=coprow.table.Columns.getColumn(i);
             if (column.getObject(coprow.index) != null)
             {
                 this.table.Columns.getColumn(i).setObject(this.size(), column.getObject(coprow.index));
             }
             else
             {
                 this.table.Columns.getColumn(i).setObject(this.size(), null);
             }
         }
         for (int j = coprow.table.Columns.size(); j < count; j++)
         {
         	this.table.Columns.getColumn(j).setObject(this.size(), null);
         }
        DataRow row = new DataRow(table, this.size());
    	super.add(row);
		return row;
    }
    public DataRow getRow(int i) throws Exception
    {
    	DataRow datarow;
    	try
        {
    		datarow = (DataRow)super.get(i);
        }
    	catch (Exception x)
        {
            throw new Exception("此索引处不存在列");
        }
            return datarow;
  
    }
}