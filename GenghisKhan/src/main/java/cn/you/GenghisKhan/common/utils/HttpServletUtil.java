package cn.you.GenghisKhan.common.utils;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class HttpServletUtil {
    public static Map<String, Object> requestParamsToMap(HttpServletRequest request) {
        Map<String, Object> param = new HashMap<>();
        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
            String name = (String) e.nextElement();
            if (null != request.getParameter(name)) {
                try {
                    param.put(StringUtils.trim(name),java.net.URLDecoder.decode(request.getParameter(name).toString(), "UTF-8").trim());
                } catch (Exception e1) {
                    String out=String.format("参数转换异常;%s",e1);
                    System.out.println(out);
                }
            }
        }
        return param;
    }
}
