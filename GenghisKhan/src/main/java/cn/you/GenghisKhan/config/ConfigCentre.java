package cn.you.GenghisKhan.config;

import cn.you.GenghisKhan.common.extension.SPI;
import cn.you.GenghisKhan.config.bean.AppBean;
import cn.you.GenghisKhan.db.relation.bean.DatabaseBean;
import cn.you.GenghisKhan.db.relation.bean.TenantBean;
import java.util.List;

/**
 * author:wl
 */
@SPI
public interface ConfigCentre {
    List<TenantBean> getTenantBeans();
    DatabaseBean getDatabaseBean(String tag);
    AppBean getAppBean(String appName);
    AppBean getAppBean();
}
