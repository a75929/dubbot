package cn.you.GenghisKhan.common.dataset;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
/**
 * author:wuliang
 */
public class DataSet extends ResultParse implements ISerialize{
	DataTable table;
	 public List<DataTable>Tables=   new ArrayList<DataTable>();
	 public DataSet(byte[] bytes) throws Exception{
		Parse(bytes);
	 }
	 public DataSet()
	 {
		 
	 }
	 @Override
 	 public void BeginTable(TableArgs args)
     {
         table = new DataTable(args.tableName,this);
     }
	 @Override
     public void OnColumn(ColumnArgs args)
     {
         table.Columns.Add(args.ColumnName, args.type);
 
     }
	 @Override
     public void OnRow(RowArgs args) throws Exception
     {
         table.Rows.Add(args.Values);
     }
	 @Override
     public  void EndTable(TableArgs args)
     {
         this.Tables.add(table);
     }
	 public List<DataTable> getTables() {
			return Tables;
	}
	 public DataTable getTable(int index){
		 return Tables.get(index);
	 }
	 public DataTable getTable(String tableName)
	 {
		 for (Object c : Tables.toArray()) {
			 DataTable table = (DataTable) c;
				if (table.getTableName().equals(tableName))
					return table;
			}
		 return null;
	 }
	 /**
	  * 复制数据集Copy
	  * */
	 public DataSet Copy() throws Exception{
		 DataSet dsCopy=new DataSet();
		 for(int i=0;i<this.Tables.size();i++){
			 dsCopy.Tables.add(this.Tables.get(i).Copy());
		 }
		 return dsCopy;
	 }
	public byte[] ToArray() throws IOException,Exception
	 {
		 int tcount=Tables.size();
		 ByteArrayOutputStream stream=new ByteArrayOutputStream();
		 stream.write(Record.Version);//写入版本号
		 for(int i=0;i<tcount;i++)
		 {
			 String tablename=Tables.get(i).getTableName();
			 int ccount = Tables.get(i).Columns.size();
			 int rowCount = Tables.get(i).Rows.size();
			 Record.WriteTypeValue(stream, DataType.String,tablename);
			 Record.WriteTypeValue(stream, DataType.Int32, ccount);//写入列数	 
			 Record.WriteTypeValue(stream, DataType.Int32, rowCount);//写入记录数
             DataType[] columnTypes = new DataType[ccount];
             for (int fieldIndex = 0; fieldIndex < ccount; fieldIndex++)
             {
                 DataType type = Record.TypeToDataType(Tables.get(i).Columns.getColumn(fieldIndex).type);
                 columnTypes[fieldIndex] = type;
                 Record.WriteTypeValue(stream, DataType.String, Tables.get(i).Columns.getColumn(fieldIndex).columnName);//写入字段名称

                 Record.WriteTypeValue(stream, DataType.Int32, type.ordinal());//写入类型
             }
             for (Object c : Tables.get(i).Rows.toArray())
             {
            	 DataRow row=(DataRow)c;
                 for (int colIndex = 0; colIndex < ccount; colIndex++)
                 {
                     Record.WriteTypeValue(stream, columnTypes[colIndex], row.getObject(colIndex));
                 }
             }
             if(i+1<tcount)
             {
            	 stream.write((byte)1);
             }
             else
             {
            	 stream.write((byte)0);
             }
		 }
		return stream.toByteArray();
		 
	 }
	@Override
	public boolean FromBytes(byte[] bytes, RefObject<String> errmsg) {
		try {
			Parse(bytes);
		} catch (Exception e) {
			errmsg.argvalue="字节序列化为DataSet失败,错误信息:"+e;
			return false;
		}
		return true;
	}

}
