package cn.you.GenghisKhan.config.db;
import cn.you.GenghisKhan.db.relation.AdvisorDaoSupport;
import cn.you.GenghisKhan.db.relation.DynamicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class MybatisConfig extends DataSourceConfig{

    @Bean
    public SqlSessionFactory sqlSessionFactory(DynamicDataSource dynamicDataSource) throws Exception {
        SqlSessionFactoryBean fb = new SqlSessionFactoryBean();
        fb.setDataSource(dynamicDataSource);// 指定数据源(这个必须有，否则报错)
        fb.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:/mapper/**/*.xml"));
        fb.setConfigLocation(new PathMatchingResourcePatternResolver().getResources("classpath*:mybatis.xml")[0]);
        return fb.getObject();
    }

    @Bean
    public  SqlSessionDaoSupport dataSourceSupport(SqlSessionFactory sqlSessionFactory){
        AdvisorDaoSupport dss=new AdvisorDaoSupport();
        dss.setSqlSessionFactory(sqlSessionFactory);
        return dss;
    }
}
