package cn.you.GenghisKhan.config.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
public class GlobalConfig {
    private static final Logger logger = LoggerFactory.getLogger(GlobalConfig.class);

    private static Map<String, String> globalMap = new HashMap<>();

    static {
        loadGlobalConfig();
    }
    private static void loadGlobalConfig(){
        try {
            String filePath="/etc/global.config";
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")){
                filePath="C:\\etc\\global.config";
            }
            File file=new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line = null;
            while ((line = br.readLine()) != null) {
                if (line.contains("=")){
                    String keyvalue[]=line.split("=",2);
                    String key=keyvalue[0];
                    String value=keyvalue[1];
                    globalMap.put(key,value);
                }
            }
            br.close();
        } catch (Exception e) {
            logger.error("请检查C:\\etc\\global.config或者/etc/global.config 文件是否存在，错误：{}",e);
        }
    }
    public static String getConfig(String key) {
        if (null!=globalMap.get(key)){
            return globalMap.get(key).toString();
        }
        return null;
    }
}
