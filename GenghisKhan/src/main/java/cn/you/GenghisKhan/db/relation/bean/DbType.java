package cn.you.GenghisKhan.db.relation.bean;

public enum DbType {
    SqlServer(1),Mysql(2),Oracal(3);
    int value=0;
    DbType(int value)
    {
        this.value=value;
    }
    DbType toEnum(int value)
    {
        return values()[value];
    }
    public int getValue() {
        return value;
    }
}
