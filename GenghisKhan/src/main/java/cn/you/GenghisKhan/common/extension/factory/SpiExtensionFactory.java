package cn.you.GenghisKhan.common.extension.factory;


import cn.you.GenghisKhan.common.extension.ExtensionFactory;
import cn.you.GenghisKhan.common.extension.ExtensionLoader;
import cn.you.GenghisKhan.common.extension.SPI;

/**
 * SpiExtensionFactory
 * 
 * @author william.liangf
 */
public class SpiExtensionFactory implements ExtensionFactory {

    public <T> T getExtension(Class<T> type, String name) {
        if (type.isInterface() && type.isAnnotationPresent(SPI.class)) {
            ExtensionLoader<T> loader = ExtensionLoader.getExtensionLoader(type);
            if (loader.getSupportedExtensions().size() > 0) {
                return loader.getAdaptiveExtension();
            }
        }
        return null;
    }

}
