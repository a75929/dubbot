package cn.you.GenghisKhan.db.relation.mybatis;

import cn.you.GenghisKhan.common.spring.SpringContextHolder;
import cn.you.GenghisKhan.db.relation.AdvisorDaoSupport;
import org.mybatis.spring.support.SqlSessionDaoSupport;

public class DS{
    AdvisorDaoSupport advisorDaoSupport;
    public DS(AdvisorDaoSupport advisorDaoSupport)
    {
        this.advisorDaoSupport=advisorDaoSupport;
    }
    public  Mapper mapper(){
        Mapper dao=new Mapper(advisorDaoSupport);
        return dao;
    }
    public BizMapper mapper(String daoName){
        BizMapper dao=new BizMapper(advisorDaoSupport,daoName);
        return dao;
    }
}
