package cn.you.GenghisKhan.db.relation;

public class DynamicDataSourceHolder{
    public static final ThreadLocal<String> holder = new ThreadLocal<String>();//线程局部变量

    public static void putDataSource(String name) {
        holder.set(name);
    }

    public static String getDataSouce() {
        return holder.get();
    }
}
