package cn.you.GenghisKhan.rpc.generic;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * author :wl
 * 通用调用服务
 */
public interface GenericService {
    /**
     * 调用返回原始对象
     */
    Object $invoke (String service, String version, String method, HttpServletRequest request);
    /**
     * 调用返回Json对象
     */
    String $invokeToJson (String service, String version, String method, HttpServletRequest request);
    /**
     * 调用返回原始对象
     */
    Object $invoke (String service, String version, String method, Map<String, Object> paramsMap);

    /**
     * 调用返回Json对象
     */
    String $invokeToJson (String service, String version, String method, Map<String, Object> paramsMap);

}
