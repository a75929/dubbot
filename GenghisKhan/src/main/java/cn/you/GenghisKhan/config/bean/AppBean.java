package cn.you.GenghisKhan.config.bean;

import cn.you.GenghisKhan.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AppBean {
    private static final Logger logger = LoggerFactory.getLogger(AppBean.class);

    private String appName;

    private String appTitle;

    private Integer rpcPort;

    private String rpcProtocol;

    private String rpcScan;

    private String zkAddress;

    private String logLevel;

    private String logProperties;

    private Integer tomcatPort;

    private Integer rpcTimeout;

    private Integer tomcatMaxconnections;

    private Integer tomcatAcceptcount;

    private Integer tomcatMaxthreads;

    private Integer tomcatMinsparethreads;

    private Integer tomcatConnectiontimeout;

    private String relationDef;

    private String properties;
    private Map<String,Object> params;

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public Object getParam(String name) {
        if(!StringUtils.isEmpty(name)
                &&params!=null
                &&params.containsKey(name)
                )
        {
            return params.get(name);
        }
        logger.error("应用{},没有读取到属性参数：{}",appName,name);
        return null;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public Integer getRpcPort() {
        return rpcPort;
    }

    public void setRpcPort(Integer rpcPort) {
        this.rpcPort = rpcPort;
    }

    public String getRpcProtocol() {
        return rpcProtocol;
    }

    public void setRpcProtocol(String rpcProtocol) {
        this.rpcProtocol = rpcProtocol;
    }

    public String getRpcScan() {
        return rpcScan;
    }

    public void setRpcScan(String rpcScan) {
        this.rpcScan = rpcScan;
    }

    public String getZkAddress() {
        return zkAddress;
    }

    public void setZkAddress(String zkAddress) {
        this.zkAddress = zkAddress;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getLogProperties() {
        return logProperties;
    }

    public void setLogProperties(String logProperties) {
        this.logProperties = logProperties;
    }

    public Integer getTomcatPort() {
        return tomcatPort;
    }

    public void setTomcatPort(Integer tomcatPort) {
        this.tomcatPort = tomcatPort;
    }

    public Integer getTomcatMaxconnections() {
        return tomcatMaxconnections;
    }

    public void setTomcatMaxconnections(Integer tomcatMaxconnections) {
        this.tomcatMaxconnections = tomcatMaxconnections;
    }

    public Integer getTomcatAcceptcount() {
        return tomcatAcceptcount;
    }

    public void setTomcatAcceptcount(Integer tomcatAcceptcount) {
        this.tomcatAcceptcount = tomcatAcceptcount;
    }

    public Integer getTomcatMaxthreads() {
        return tomcatMaxthreads;
    }

    public void setTomcatMaxthreads(Integer tomcatMaxthreads) {
        this.tomcatMaxthreads = tomcatMaxthreads;
    }

    public Integer getTomcatMinsparethreads() {
        return tomcatMinsparethreads;
    }

    public void setTomcatMinsparethreads(Integer tomcatMinsparethreads) {
        this.tomcatMinsparethreads = tomcatMinsparethreads;
    }

    public Integer getTomcatConnectiontimeout() {
        return tomcatConnectiontimeout;
    }

    public void setTomcatConnectiontimeout(Integer tomcatConnectiontimeout) {
        this.tomcatConnectiontimeout = tomcatConnectiontimeout;
    }

    public Integer getRpcTimeout() {
        if(rpcTimeout==null||rpcTimeout==0)
            rpcTimeout=3000;
        return rpcTimeout;
    }

    public void setRpcTimeout(Integer rpcTimeout) {
        this.rpcTimeout = rpcTimeout;
    }

    public String getRelationDef() {
        return relationDef;
    }

    public void setRelationDef(String relationDef) {
        this.relationDef = relationDef;
    }
}