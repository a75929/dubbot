package cn.you.GenghisKhan.common.internal.ex;

public class ConvertException extends RuntimeException {
    private static final long  serivalVersionUID=11L;
    private String errmsg;
    public ConvertException(String errmsg) {
        this.errmsg = errmsg;
    }
    public String getErrmsg() {
        return errmsg;
    }
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}
