package cn.you.GenghisKhan.config;

import cn.you.GenghisKhan.config.bean.AppBean;
import cn.you.GenghisKhan.config.utils.GlobalConfig;
import cn.you.GenghisKhan.db.relation.bean.DatabaseBean;
import cn.you.GenghisKhan.db.relation.bean.TenantBean;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MongoConfigCentare extends AbstractConfigCentre{
    private   MongoClient mongoClient;
    public MongoConfigCentare()
    {
        mongoClient =new MongoClient(GlobalConfig.getConfig("mongo.host"),
                Integer.valueOf(GlobalConfig.getConfig("mongo.port")));
    }


    protected    <T>MongoCollection<Document> getCollection(Class<T> cls,String value){
        return mongoClient.getDatabase(GlobalConfig.getConfig("mongo.db")).getCollection(value);
    }
    @Override
    List<TenantBean> createTenantBeans () {
        return new ArrayList<>();
    }
    @Override
    DatabaseBean createDatabaseBean (String tag) {
        return new DatabaseBean();
    }

    @Override
    Map<String, DatabaseBean> createDatabaseBeanMap () {
        return null;
    }

    @Override
    AppBean createAppBean(String appName) {
        return null;
    }


}
