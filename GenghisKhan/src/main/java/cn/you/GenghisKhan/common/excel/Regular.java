package cn.you.GenghisKhan.common.excel;

public enum Regular {
    制表符("[\\s\\S]*\t[\\s\\S]*"),
    回车符("[\\s\\S]*\r[\\s\\S]*"),
    换行符("[\\s\\S]*\n[\\s\\S]*"),
    换页符("[\\s\\S]*\f[\\s\\S]*");
    private final String value;
    private Regular (String value) {
        this.value = value;
    }
    public String value ()
    {
        return value;
    }
}