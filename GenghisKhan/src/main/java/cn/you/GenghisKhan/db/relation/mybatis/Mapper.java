package cn.you.GenghisKhan.db.relation.mybatis;

import cn.you.GenghisKhan.db.relation.AdvisorDaoSupport;
import cn.you.GenghisKhan.db.relation.bean.pager.DataPager;
import cn.you.GenghisKhan.db.relation.bean.pager.Pager;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public class Mapper {
    protected AdvisorDaoSupport advisorDaoSupport;

    public Mapper(AdvisorDaoSupport advisorDaoSupport){
        this.advisorDaoSupport= advisorDaoSupport;
        if(this.advisorDaoSupport!=null)
        {
           // advisorDaoSupport.getSqlSession().clearCache();//清空缓存
        }
    }
    /**
     * 通过主键查询
     * @param cls
     * @param key
     * @param <M>
     * @return
     */
    public <M> M  selectByPrimaryKey(Class<M> cls,Object key){
        String selectId= MybatisUtil.getNamespace(cls)+ ExampleEnum.selectByPrimaryKey;
        M m=this.advisorDaoSupport.selectOne(selectId, key);
        return m;
    }



    /**
     * 通过expample查询列表
     * @param example
     * @param <E>
     * @return
     */
    public <E> List<E> selectByExample(Object example){
        String selectId= MybatisUtil.getNamespace(example.getClass())+ ExampleEnum.selectByExample;
        List<E> ls=this.advisorDaoSupport.selectList(selectId, example);
        return ls;
    }

    /**
     * 通过example查询类别数据
     * @param example
     * @param <E>
     * @return
     */
    public <E> List<E>  selectByExampleWithBLOBs(Object example){
        String selectId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.selectByExampleWithBLOBs;
        List<E> ls=this.advisorDaoSupport.selectList(selectId, example);
        return ls;
    }

    /**
     * 通过example查询列表第一个
     * @param example
     * @param <M>
     * @return
     */
    public <M> M selectFirstByExample(Object example){
        String selectId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.selectByExample;
        List<M> ls=this.advisorDaoSupport.selectList(selectId, example);
        if(ls.size()>0){
            return  ls.get(0);
        }
        return null;
    }

    /**
     * 通过example查询总数
     * @param example
     * @return
     */
    public int countByExample(Object example){
        String countId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.countByExample;
        int count=this.advisorDaoSupport.selectCount(countId, example);
        return count;
    }

    /**
     * 通过example删除
     * @param example
     * @return
     */
    public int deleteByExample(Object example){
        String deleteId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.deleteByExample;
        int count=this.advisorDaoSupport.delete(deleteId, example);
        return count;
    }

    /**
     * 插入数据
     * @param parameter
     * @return
     */
    public int  insert(Object parameter){
        String insertId= MybatisUtil.getNamespace(parameter.getClass())+ExampleEnum.insert;
        int count=this.advisorDaoSupport.insert(insertId, parameter);
        return count;
    }

    /**
     * 有选择性插入数据
     * @param parameter
     * @return
     */
    public int insertSelective(Object parameter){
        String insertId= MybatisUtil.getNamespace(parameter.getClass())+ExampleEnum.insertSelective;
        int count=this.advisorDaoSupport.insert(insertId, parameter);
        return count;
    }

    /**
     * 通过example更新
     * @param record
     * @param example
     * @return
     */
    public int updateByExample(Object record, Object example) {
        MapperMethod.ParamMap<Object> pm=new MapperMethod.ParamMap<Object>();
        pm.put("record", record);
        pm.put("example", example);
        String updateId= MybatisUtil.getNamespace(record.getClass())+ExampleEnum.updateByExample;
        int obj=this.advisorDaoSupport.update(updateId, pm);
        return obj;
    }

    /**
     * 有选择性的通过example更新
     * @param record
     * @param example
     * @return
     */
    public int updateByExampleSelective(Object record,Object example){
        MapperMethod.ParamMap<Object> pm=new MapperMethod.ParamMap<Object>();
        pm.put("record", record);
        pm.put("example", example);
        String updateId= MybatisUtil.getNamespace(record.getClass())+ExampleEnum.updateByExampleSelective;
        int obj=this.advisorDaoSupport.update(updateId, pm);
        return obj;
    }

    /**
     * 有选择性的通过主键更新
     * @param record
     * @return
     */
    public int updateByPrimaryKeySelective(Object record) {
        String updateId= MybatisUtil.getNamespace(record.getClass())+ExampleEnum.updateByPrimaryKeySelective;
        int obj=this.advisorDaoSupport.update(updateId, record);
        return obj;
    }

    /**
     * 通过主键更新
     * @param record
     * @param <M>
     * @return
     */
    public <M> int updateByPrimaryKey(Object record) {
        String updateId= MybatisUtil.getNamespace(record.getClass())+ExampleEnum.updateByPrimaryKey;
        int obj=this.advisorDaoSupport.update(updateId, record);
        return obj;
    }

    /**
     * 通过主键删除
     * @param record
     * @param <M>
     * @return
     */
    public <M> int deleteByPrimaryKey(Object record){
        String deleteId= MybatisUtil.getNamespace(record.getClass())+ExampleEnum.deleteByPrimaryKey;
        int count=this.advisorDaoSupport.delete(deleteId, record);
        return count;
    }

    /**
     * 通过Example分页查询
     * @param example
     * @param page 页码
     * @param size 大小
     * @param <E>
     * @param <M>
     * @return
     */
    public <E,M> List<E> selectByExampleForPage(M example, int page,int size){
        String selectId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.selectByExample;
        RowBounds rowBounds = new RowBounds(page, size);
        List<E> ls=this.advisorDaoSupport.selectLimitedList(selectId, example, rowBounds);
        return ls;
    }

    /**
     * 通过Example分页查询 DataPager
     * @param example
     * @param pager
     * @param <T>
     * @param <M>
     * @return
     */
    public <T,M> DataPager<T> selectDataPager (M example, Pager pager){
        String selectId= MybatisUtil.getNamespace(example.getClass())+ExampleEnum.selectByExample;
        RowBounds rowBounds = new RowBounds(pager.getPage(), pager.getSize());
        List<T> ls=this.advisorDaoSupport.selectLimitedList(selectId, example, rowBounds);

        DataPager dataPager=new DataPager(pager);
        dataPager.setRows(ls);
        PageInfo page = new PageInfo(ls);
        dataPager.setTotalPage(page.getPages());
        dataPager.setTotal(page.getTotal());
        return  dataPager;
    }
}