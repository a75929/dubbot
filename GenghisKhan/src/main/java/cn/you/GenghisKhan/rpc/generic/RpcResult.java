package cn.you.GenghisKhan.rpc.generic;

import java.io.Serializable;

public class RpcResult<T> implements Serializable {
    public boolean success;
    public String msg;
    public int code;
    public T result;

    public RpcResult() {
        this.success=true;
    }

    public RpcResult(boolean success, int code, String msg, T result) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.result = result;
    }

    public RpcResult(int code, String msg) {
        this.success = false;
        this.code = code;
        this.msg = msg;
        this.result = null;
    }

    public RpcResult(T result) {
        this.success = true;
        this.code = 0;
        this.msg = "成功！";
        this.result = result;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;this.success = false;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;this.success = false;
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}