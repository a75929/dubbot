package cn.you.GenghisKhan.rpc.generic.dubbo;

import cn.you.GenghisKhan.common.extension.ExtensionLoader;
import cn.you.GenghisKhan.config.ConfigCentre;
import cn.you.GenghisKhan.config.bean.AppBean;
import cn.you.GenghisKhan.rpc.generic.AbstractGeneric;
import cn.you.GenghisKhan.rpc.generic.ErrorCode;
import cn.you.GenghisKhan.rpc.generic.RpcResult;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.*;
import com.alibaba.dubbo.config.spring.extension.SpringExtensionFactory;
import com.alibaba.dubbo.config.support.Parameter;
import com.alibaba.dubbo.rpc.service.GenericService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * dubbo 调用服务
 */
public class DubboService extends AbstractGeneric<ReferenceConfig> implements ApplicationContextAware, InitializingBean, DisposableBean {
    private transient ApplicationContext applicationContext;
    // 缺省配置
    private ConsumerConfig consumer;
    // 应用信息
    protected ApplicationConfig application;
    // 模块信息
    protected ModuleConfig module;
    // 注册中心
    protected List<RegistryConfig> registries;
    // 服务监控
    protected MonitorConfig monitor;

    public DubboService() {
        super();

    }

    public Object invoke(String interfaceName, String version, String methodName, Map<String, Object> paramsMap) {
        ReferenceConfig reference = getReferenceConfig(interfaceName, version);
        if (null != reference) {
            GenericService genericService = (GenericService) reference.get();
            Object[] paramObject = null;
            if (!CollectionUtils.isEmpty(paramsMap)) {
                paramObject = new Object[1];
                paramObject[0] = paramsMap;
            }
            Object resultParam = null;
            resultParam = genericService.$invoke(methodName, null, paramObject);
            return resultParam;
        }
        return null;
    }

    @Override
    protected String invokeToJson(String interfaceName, String version, String methodName, Map<String, Object> paramsMap) {
        RpcResult rpcResult;
        try {
            Object resultParam = invoke(methodName, version, methodName, paramsMap);
            if (resultParam instanceof RpcResult) {
                rpcResult = (RpcResult) resultParam;
            } else {
                rpcResult = new RpcResult();
                rpcResult.setResult(resultParam);
            }
        } catch (com.alibaba.dubbo.rpc.RpcException ex) {
            rpcResult = new RpcResult();
            switch (ex.getCode()) {
                case com.alibaba.dubbo.rpc.RpcException.NETWORK_EXCEPTION:
                    rpcResult.setCode(ErrorCode.NETWORK_EXCEPTION);
                    break;
                case com.alibaba.dubbo.rpc.RpcException.TIMEOUT_EXCEPTION:
                    rpcResult.setCode(ErrorCode.TIMEOUT_EXCEPTION);
                    break;
                case com.alibaba.dubbo.rpc.RpcException.SERIALIZATION_EXCEPTION:
                    rpcResult.setCode(ErrorCode.SERIALIZATION_EXCEPTION);
                    break;
                case com.alibaba.dubbo.rpc.RpcException.FORBIDDEN_EXCEPTION:
                    rpcResult.setCode(ErrorCode.FORBIDDEN_EXCEPTION);
                    break;
                case com.alibaba.dubbo.rpc.RpcException.BIZ_EXCEPTION:
                    rpcResult.setCode(ErrorCode.BIZ_EXCEPTION);
                    break;
                default:
                    rpcResult.setCode(ErrorCode.UNKNOWN_EXCEPTION);
                    break;
            }
        }
        return JSONObject.toJSONString(rpcResult);
    }

    private ReferenceConfig getReferenceConfig(String interfaceName, String version) {
        String referenceKey = interfaceName;
        ReferenceConfig referenceConfig = referenceCache.get(referenceKey);
        if (null == referenceConfig) {
            try {
                AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
                referenceConfig = new ReferenceConfig<GenericService>();
                referenceConfig.setApplication(application);
                referenceConfig.setRegistries(registries);
                referenceConfig.setConsumer(consumer);
                referenceConfig.setMonitor(monitor);
                referenceConfig.setInterface(interfaceName);
                referenceConfig.setTimeout(appbean.getRpcTimeout());
                referenceConfig.setRetries(0);
                if (StringUtils.isNotEmpty(version)) {
                    referenceConfig.setVersion(version);
                }
                referenceConfig.setGeneric(true);
                referenceCache.put(referenceKey, referenceConfig);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return referenceConfig;
    }

    @Parameter(excluded = true)
    public boolean isSingleton() {
        return true;
    }

    @SuppressWarnings({"unchecked"})
    public void afterPropertiesSet() throws Exception {
        if (consumer == null) {
            Map<String, ConsumerConfig> consumerConfigMap = applicationContext == null ? null : BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, ConsumerConfig.class, false, false);
            if (consumerConfigMap != null && consumerConfigMap.size() > 0) {
                ConsumerConfig consumerConfig = null;
                for (ConsumerConfig config : consumerConfigMap.values()) {
                    if (config.isDefault() == null || config.isDefault().booleanValue()) {
                        if (consumerConfig != null) {
                            throw new IllegalStateException("Duplicate consumer configs: " + consumerConfig + " and " + config);
                        }
                        consumerConfig = config;
                    }
                }
                if (consumerConfig != null) {
                    consumer = consumerConfig;
                    consumer.setCheck(false);
                }
            }
        }
        if (application == null
                && (this.consumer == null || consumer.getApplication() == null)) {
            Map<String, ApplicationConfig> applicationConfigMap = applicationContext == null ? null : BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, ApplicationConfig.class, false, false);
            if (applicationConfigMap != null && applicationConfigMap.size() > 0) {
                ApplicationConfig applicationConfig = null;
                for (ApplicationConfig config : applicationConfigMap.values()) {
                    if (config.isDefault() == null || config.isDefault().booleanValue()) {
                        if (applicationConfig != null) {
                            throw new IllegalStateException("Duplicate application configs: " + applicationConfig + " and " + config);
                        }
                        applicationConfig = config;
                    }
                }
                if (applicationConfig != null) {
                    this.application = applicationConfig;
                }
            }
        }
        if (module == null
                && (consumer == null || consumer.getModule() == null)) {
            Map<String, ModuleConfig> moduleConfigMap = applicationContext == null ? null : BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, ModuleConfig.class, false, false);
            if (moduleConfigMap != null && moduleConfigMap.size() > 0) {
                ModuleConfig moduleConfig = null;
                for (ModuleConfig config : moduleConfigMap.values()) {
                    if (config.isDefault() == null || config.isDefault().booleanValue()) {
                        if (moduleConfig != null) {
                            throw new IllegalStateException("Duplicate module configs: " + moduleConfig + " and " + config);
                        }
                        moduleConfig = config;
                    }
                }
                if (moduleConfig != null) {
                    module = moduleConfig;
                }
            }
        }
        if ((registries == null || registries.size() == 0)
                && (consumer == null || consumer.getRegistries() == null || consumer.getRegistries().size() == 0)
                && (application == null || application.getRegistries() == null || application.getRegistries().size() == 0)) {
            Map<String, RegistryConfig> registryConfigMap = applicationContext == null ? null : BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, RegistryConfig.class, false, false);
            if (registryConfigMap != null && registryConfigMap.size() > 0) {
                List<RegistryConfig> registryConfigs = new ArrayList<RegistryConfig>();
                for (RegistryConfig config : registryConfigMap.values()) {
                    if (config.isDefault() == null || config.isDefault().booleanValue()) {
                        registryConfigs.add(config);
                    }
                }
                if (registryConfigs != null && registryConfigs.size() > 0) {
                    this.registries = registryConfigs;
                }
            }
        }
        if (monitor == null
                && (consumer == null || consumer.getMonitor() == null)
                && (application == null || application.getMonitor() == null)) {
            Map<String, MonitorConfig> monitorConfigMap = applicationContext == null ? null : BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, MonitorConfig.class, false, false);
            if (monitorConfigMap != null && monitorConfigMap.size() > 0) {
                MonitorConfig monitorConfig = null;
                for (MonitorConfig config : monitorConfigMap.values()) {
                    if (config.isDefault() == null || config.isDefault().booleanValue()) {
                        if (monitorConfig != null) {
                            throw new IllegalStateException("Duplicate monitor configs: " + monitorConfig + " and " + config);
                        }
                        monitorConfig = config;
                    }
                }
                if (monitorConfig != null) {
                    this.monitor = monitorConfig;
                }
            }
        }
        /*
        Boolean b = isInit();
        if (b == null && consumer != null) {
            b = consumer.isInit();
        }
        if (b != null && b.booleanValue()) {
            getObject();
        }*/
    }


    public void destroy() throws Exception {

    }


    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
        SpringExtensionFactory.addApplicationContext(applicationContext);
    }


}
