package cn.you.GenghisKhan.db.relation.bean;

import java.util.List;

public class TenantBean {
    String dbTag;//数据库标签
    LookUpType type;
    long start;//租户开始ID
    long end;//租户结束ID
    List<Long> specifyList;//指定租户列表

    public String getDbTag () {
        return dbTag;
    }

    public void setDbTag (String dbTag) {
        this.dbTag = dbTag;
    }

    public LookUpType getType () {
        return type;
    }

    public void setType (LookUpType type) {
        this.type = type;
    }

    public long getStart () {
        return start;
    }

    public void setStart (long start) {
        this.start = start;
    }

    public long getEnd () {
        return end;
    }

    public void setEnd (long end) {
        this.end = end;
    }

    public List<Long> getSpecifyList () {
        return specifyList;
    }

    public void setSpecifyList (List<Long> specifyList) {
        this.specifyList = specifyList;
    }
}
