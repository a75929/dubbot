package cn.you.GenghisKhan.common.extension;
import java.lang.annotation.*;
@Documented   
@Retention(RetentionPolicy.RUNTIME)   
@Target({ElementType.TYPE})
public @interface Must {
}
