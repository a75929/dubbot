package cn.you.GenghisKhan.common.parser;
import java.util.List;

public abstract interface Reader
{
  public abstract boolean hasReturnField (Object paramObject);
  
  public abstract Object getPrimitiveObject (Object paramObject);
  
  public abstract Object getObject (Object paramObject, Class<?> paramClass) throws Exception;
  
  public abstract List<?> getListObjects (Object paramObject1, Object paramObject2, Class<?> paramClass, List<ObjectConfig> objectlist) throws Exception;
}
