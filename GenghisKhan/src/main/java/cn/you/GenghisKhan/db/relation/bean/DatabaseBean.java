package cn.you.GenghisKhan.db.relation.bean;

public class DatabaseBean {
    private String name;//数据库名称
    private int dbType;//1：mysql，2：sqlserver，3：Oracal
    private String host;
    private int port;
    private String username;
    private String password;
    private int initialSize;
    private int maxActive;
    private int minIdle;
    private int maxWait;
    /**
     * 数据库名称
     * @return
     */
    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public int getDbType() {
        return dbType;
    }

    public void setDbType(int dbType) {
        this.dbType = dbType;
    }

    public String getHost () {
        return host;
    }

    public void setHost (String host) {
        this.host = host;
    }

    public int getPort () {
        return port;
    }

    public void setPort (int port) {
        this.port = port;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername (String username) {
        this.username = username;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public int getInitialSize () {
        return initialSize;
    }

    public void setInitialSize (int initialSize) {
        this.initialSize = initialSize;
    }

    public int getMaxActive () {
        return maxActive;
    }

    public void setMaxActive (int maxActive) {
        this.maxActive = maxActive;
    }

    public int getMinIdle () {
        return minIdle;
    }

    public void setMinIdle (int minIdle) {
        this.minIdle = minIdle;
    }

    public int getMaxWait () {
        return maxWait;
    }

    public void setMaxWait (int maxWait) {
        this.maxWait = maxWait;
    }
}
