package cn.you.GenghisKhan.rpc.generic;
import cn.you.GenghisKhan.common.utils.HttpServletUtil;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractGeneric<T> implements GenericService {
    protected   Map<String, T> referenceCache = new ConcurrentHashMap();
    protected abstract Object invoke (String interfaceName,  String version,String methodName, Map<String, Object> paramsMap);
    protected abstract String invokeToJson (String interfaceName,  String version,String methodName, Map<String, Object> paramsMap);
    public Object $invoke (String service, String version, String method, HttpServletRequest request)
    {
        Map<String,Object> paramsMap=HttpServletUtil.requestParamsToMap(request);
        return invoke(service,version,method,paramsMap);
    }
    public String $invokeToJson (String service, String version, String method, HttpServletRequest request)
    {
        Map<String,Object> paramsMap= HttpServletUtil.requestParamsToMap(request);
        return invokeToJson(service,version,method,paramsMap);
    }
    @Override
    public Object $invoke(String service, String version, String method, Map<String, Object> paramsMap) {
        return invoke(service,version,method,paramsMap);
    }

    @Override
    public String $invokeToJson(String service, String version, String method, Map<String, Object> paramsMap) {
        return invokeToJson(service,version,method,paramsMap);
    }
}
