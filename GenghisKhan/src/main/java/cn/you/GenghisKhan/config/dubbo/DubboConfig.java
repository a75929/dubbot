package cn.you.GenghisKhan.config.dubbo;

import cn.you.GenghisKhan.common.extension.ExtensionLoader;
import cn.you.GenghisKhan.common.utils.PropertyUtil;
import cn.you.GenghisKhan.config.ConfigCentre;
import cn.you.GenghisKhan.config.bean.AppBean;
import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.MonitorConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import org.springframework.context.annotation.Bean;
public class DubboConfig {
    @Bean
    public RegistryConfig registry () {
        AppBean appbean = ExtensionLoader.getExtensionLoader(ConfigCentre.class).getAdaptiveExtension().getAppBean();
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(appbean.getZkAddress());
        registryConfig.setProtocol("zookeeper");
        registryConfig.setTimeout(5000);
        return registryConfig;
    }
    @Bean
    public ApplicationConfig application () {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName(PropertyUtil.getProperty("app.name"));
        return applicationConfig;
    }
    @Bean
    public MonitorConfig monitorConfig(){
        MonitorConfig monitorConfig = new MonitorConfig();
        monitorConfig.setProtocol("registry");
        return monitorConfig;
    }
}