package cn.you.GenghisKhan.common.parser;

import java.util.ArrayList;
import java.util.List;

public class ObjectConfig {
	String filed;
	String node;
	List<ObjectConfig>childnodes=new ArrayList();
	public String getFiled() {
		return filed;
	}
	public void setFiled(String filed) {
		this.filed = filed;
	}
	public String getNode() {
		return node;
	}
	public void setNode(String node) {
		this.node = node;
	}
	public List<ObjectConfig> getChildnodes() {
		return childnodes;
	}
	public void setChildnodes(List<ObjectConfig> childnodes) {
		this.childnodes = childnodes;
	}
	/**
	 * 获取对象
	 * 
	 * @param name
	 *            对象名称
	 * @return
	 */
	public static ObjectConfig GetObjectConfig(List<ObjectConfig>objects,String name)
	{

		
		ObjectConfig need = null;
			for (Object c : objects.toArray()) {
				ObjectConfig item = (ObjectConfig) c;
				if (item.getFiled().equals(name)) {
					need = item;
					break;
				}
			}
			return need;
		 
		
	}
}
