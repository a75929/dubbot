package cn.you.GenghisKhan.db.relation.mybatis;

import cn.you.GenghisKhan.db.relation.bean.LookUpType;

import javax.sql.DataSource;

public interface IDataSource {
    DataSource getDataSource(LookUpType type,Object tag);
}
