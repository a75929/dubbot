package cn.you.GenghisKhan.common.extension.factory;

import cn.you.GenghisKhan.common.extension.ExtensionFactory;
import cn.you.GenghisKhan.common.spring.SpringContextHolder;


public class SpringExtensionFactory implements ExtensionFactory {
    @Override
    public <T> T getExtension (Class<T> type, String name) {
        return SpringContextHolder.getBean(type);
    }
}
