package cn.you.GenghisKhan.db.relation;

public class DynamicDataSource extends AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey () {
        return DynamicDataSourceHolder.getDataSouce();
    }
}
