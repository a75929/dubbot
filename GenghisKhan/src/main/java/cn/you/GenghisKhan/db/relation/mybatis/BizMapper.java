package cn.you.GenghisKhan.db.relation.mybatis;
import cn.you.GenghisKhan.db.relation.AdvisorDaoSupport;
import cn.you.GenghisKhan.db.relation.bean.pager.DataPager;
import cn.you.GenghisKhan.db.relation.bean.pager.Pager;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

public class BizMapper {
    String name;
    AdvisorDaoSupport advisorDaoSupport;
    public BizMapper(AdvisorDaoSupport advisorDaoSupport,String name){
        this.name="mapper.biz."+name;
        this.advisorDaoSupport= advisorDaoSupport;
        if(this.advisorDaoSupport!=null)
        {
             advisorDaoSupport.getSqlSession().clearCache();//清空缓存
        }
    }
    /**
     * 插入
     * @param insertId
     * @param parameter
     * @return
     */
    public int insert(String insertId, Map parameter) {
        insertId=name+"."+insertId;
        return advisorDaoSupport.insert(insertId, parameter);
    }

    /**
     * 更新
     * @param updateId
     * @param parameter
     * @return
     */
    public int update(String updateId, Map parameter) {
        updateId=name+"."+updateId;
        return advisorDaoSupport.update(updateId, parameter);
    }

    /**
     * 删除
     * @param deleteId
     * @param parameter
     * @return
     */
    public int delete(String deleteId, Map parameter) {
        deleteId=name+"."+deleteId;
        return advisorDaoSupport.delete(deleteId, parameter);
    }
    /**
     * 查询一个
     * @param selectId
     * @param parameter
     * @param <M>
     * @return
     */
    public <M> M selectOne(String selectId, Map parameter) {
        selectId=name+"."+selectId;
        return advisorDaoSupport.selectOne(selectId, parameter);
    }

    /**
     * 查询列表
     * @param selectId
     * @param parameter
     * @param <E>
     * @return
     */
    public <E> List<E> selectList(String selectId, Map parameter) {
        selectId=name+"."+selectId;
        return advisorDaoSupport.selectList(selectId, parameter);
    }

    /**
     * 查询列表的第一个
     * @param selectId
     * @param parameter
     * @param <M>
     * @return
     */
    public<M> M  selectFirstFromList(String selectId, Map parameter) {
        selectId=name+"."+selectId;
        List<M> ls=advisorDaoSupport.selectList(selectId, parameter);
        if(ls.size()>0){
            return ls.get(0);
        }
        return null;
    }


    public <E> List<E> selectListForPage(String selectId, Map parameter, int page, int size) {
        selectId=name+"."+selectId;
        RowBounds rowBounds=new RowBounds(page,size);
        return advisorDaoSupport.selectLimitedList(selectId, parameter, rowBounds);
    }

    /**
     * 分页查询
     * @param selectId
     * @param parameter
     * @param rowBounds
     * @param <E>
     * @return
     */
    public <E> List<E> selectLimitedList(String selectId,Map parameter, RowBounds rowBounds) {
        selectId=name+"."+selectId;
        return advisorDaoSupport.selectLimitedList(selectId, parameter, rowBounds);
    }

    /**
     * 分页查询 DataPager
     * @param selectId
     * @param pager
     * @param <T>
     * @return
     */
    public <T> DataPager<T> selectDataPager (String selectId, Map parameter, Pager pager){
        selectId=name+"."+selectId;
        RowBounds rowBounds = new RowBounds(pager.getPage(), pager.getSize());
        List<T> ls=advisorDaoSupport.selectLimitedList(selectId, parameter, rowBounds);

        DataPager dataPager=new DataPager(pager);
        dataPager.setRows(ls);
        PageInfo page = new PageInfo(ls);
        dataPager.setTotalPage(page.getPages());
        dataPager.setTotal(page.getTotal());
        return  dataPager;
    }

    /**
     * 查询总数
     * @param countId
     * @param parameter
     * @return
     */
    public int selectCount(String countId, Map parameter) {
        countId=name+"."+countId;
        return advisorDaoSupport.selectOne(countId, parameter);
    }
}
