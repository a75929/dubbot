package cn.you.GenghisKhan.rpc.generic;

public class ErrorCode {
    public final static int UNKNOWN_EXCEPTION=10001;//未知异常
    public final static int  NETWORK_EXCEPTION=10002;//网络连接异常
    public final static int  TIMEOUT_EXCEPTION=10003;//访问超时异常
    public final static int  BIZ_EXCEPTION=10004;//,"业务异常"
    public final static int  FORBIDDEN_EXCEPTION=10005;//,"禁止服务"
    public final static int SERIALIZATION_EXCEPTION=10006;//序列化异常
}
