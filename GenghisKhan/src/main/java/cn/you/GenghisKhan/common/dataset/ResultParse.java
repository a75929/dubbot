package cn.you.GenghisKhan.common.dataset;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Type;
public class ResultParse {
	/// <summary>
	/// 解压数据结果的事件参数
	/// </summary>
	public class TableArgs
	{
		int ResultIndex;// 查询结果索引
		int FieldCount;// 当前查询结果的字段数
		int RowCount;// 当前查询结果的行数
		boolean Cancel;// 获取或设置是否取消操作
		String tableName;//当前table的名称
		public TableArgs(int resultIndex, int fieldCount, int rowCount)
		{
			// TODO: Complete member initialization
			this.ResultIndex = resultIndex;
			this.FieldCount = fieldCount;
			this.RowCount = rowCount;
			this.Cancel = false;
		}
		public TableArgs(String name,int resultIndex, int fieldCount, int rowCount)
		{
			// TODO: Complete member initialization
			this.tableName=name;
			this.ResultIndex = resultIndex;
			this.FieldCount = fieldCount;
			this.RowCount = rowCount;
			this.Cancel = false;
		}
	}
	/// <summary>
	/// 解压数据列的事件参数
	/// </summary>
	public class ColumnArgs
	{
		String ColumnName;// 列名称
		Type type;// 列数据类型
		private boolean Cancel;// 获取或设置是否取消操作
		private int ColIndex;// 列索引
		 
	 
		 

		public ColumnArgs(String columnName, Type type, int colIndex)
		{
			// TODO: Complete member initialization
			this.ColumnName = columnName;
			this.type = type;
			this.ColIndex = colIndex;
		}
	}
	/// <summary>
	/// 解压数据行的事件参数
	/// </summary>
	public class RowArgs
	{
		private int RowIndex;// 行索引
		private boolean Cancel;// 获取或设置是否取消操作
		public Object[] Values;// 行数据
		 
	 

		public RowArgs(int rowIndex, Object[] values)
		{
			// TODO: Complete member initialization
			this.RowIndex = rowIndex;
			this.Values = values;
		}
	}
	public void BeginTable(TableArgs args) { }
	public void OnColumn(ColumnArgs args) { }
	public void OnRow(RowArgs args) throws Exception { }
	public void EndTable(TableArgs args) { }
	public boolean Parse(ByteArrayInputStream stream,int i) throws Exception
	{
		int fieldCount = 0;
		int rowCount = 0;
		String tablename=(String)Record.ReadTypeValue(stream, DataType.String);
		fieldCount=(Integer)Record.ReadTypeValue(stream, DataType.Int32);
		rowCount = (Integer)Record.ReadTypeValue(stream, DataType.Int32);
		int tableindex=i;
		TableArgs resultArgs = new TableArgs(tablename,tableindex, fieldCount, rowCount);
		BeginTable(resultArgs);
		DataType[] types = new DataType[fieldCount];
		for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++)
		{
			//读取columnName
			Object columnName = Record.ReadTypeValue(stream, DataType.String);
			int typenum=(Integer)Record.ReadTypeValue(stream, DataType.Int32);
			types[fieldIndex]=DataType.values()[typenum];
			ColumnArgs columnArgs = new ColumnArgs(columnName != null ? columnName.toString() : null, Record.DataTypeToType(types[fieldIndex]), fieldIndex);
			OnColumn(columnArgs);
		}
		for (int rowIndex = 0; rowIndex < rowCount; rowIndex++)
		{
			Object[] values = new Object[fieldCount];
			for (int fieldIndex = 0; fieldIndex < fieldCount; fieldIndex++)
			{
				values[fieldIndex]=Record.ReadTypeValue(stream, types[fieldIndex]);
			}
			RowArgs rowArgs = new RowArgs(rowIndex, values);
			OnRow(rowArgs);
		}
		TableArgs endResultArgs = new TableArgs(tableindex, fieldCount, rowCount);
		EndTable(endResultArgs);
		byte[] isnexttable = new byte[1];
		stream.read(isnexttable, 0, 1);
		if(isnexttable[0]==(byte)1)
		{
			i++;
			Parse(stream,i);
		}
		return true;
	}
	public boolean Parse(byte[] bytes) throws Exception
	{
		String err="";
		ByteArrayInputStream stream = new ByteArrayInputStream(bytes);
	 
		byte[] versionBts = new byte[1];
		if(versionBts.length!=stream.read(versionBts, 0, 1))
		{
			err="读取版本信息出错!";
			return false;
		}
		if(Record.Version!=versionBts[0])
		{
			err="不支持此版本的数据！";
			return false;
		}
		 if(bytes.length > 1){
			 Parse(stream,0);
		 }
		

		return true;
	}
}
