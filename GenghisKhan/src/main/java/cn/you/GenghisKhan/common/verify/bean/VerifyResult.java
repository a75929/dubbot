package cn.you.GenghisKhan.common.verify.bean;

public class VerifyResult {
    boolean succress;
    String msg;

    public VerifyResult() {
        succress=true;
    }

    public boolean isSuccress() {
        return succress;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.succress=false;
        this.msg = msg;
    }
}
