package cn.you.GenghisKhan.common.cmd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;
 

public class CmdManger {
	/**
	 * 获取当前的进程ID
	 * @return
	 */
	public String GetPid()
	{
		String pid="";
		String name = ManagementFactory.getRuntimeMXBean().getName();    
		System.out.println(name);    
		// get pid    
		pid = name.split("@")[0];    
		System.out.println("Pid is:" + pid);
		  MemoryMXBean mem = ManagementFactory.getMemoryMXBean();  
	        MemoryUsage heap = mem.getHeapMemoryUsage();  
	        RuntimeMXBean mxbean = ManagementFactory.getRuntimeMXBean();  
	        mxbean.getStartTime();
	        mxbean.getUptime();
	        ThreadMXBean thread = ManagementFactory.getThreadMXBean();  
	        thread.getCurrentThreadUserTime();
		return pid;
	}
	public String GetSystemMemory()
	{
		 
		    return "";  
	}
	public static void main(String[] args)
	{
		CmdManger aa=new CmdManger();
		List<String> aaa=aa.GetCmd("netstat -ano");
		for(String msg:aaa)
		{
			System.out.println(msg);	
		}
		
	}
	 
	 public List<String> GetCmd(String args) {
		 List<String> lists=new ArrayList<String>();
		  String[] cmd = new String[] { "cmd.exe", "/C", args };
		  try{
			  Process process = Runtime.getRuntime().exec(cmd);
			  BufferedReader reader = new BufferedReader(
	                    new InputStreamReader(process.getInputStream(),"GB2312"));
			  String line = null;
	            while ((line = reader.readLine()) != null) {
	            	lists.add(line);
	            }
		  }
		  catch(Exception x)
		  {
			  lists.add(x.getMessage());
		  }
		  return  lists;
	 }
	
}
