package cn.you.GenghisKhan.common.dataset;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
public class DataColumn
{
        public String columnName;//字段名
        DataTable table;//对象引用
        public Type type;//字段类型
        List<Object>data=   new ArrayList<Object>();
        public void SetTable(DataTable table)
        {
            this.table = table;
        }
        public DataColumn(String columnName, Type dataType)
        {
            this.columnName = columnName;
            this.type = dataType;
        }
        
        public DataColumn(String columnName)
        {
            this.columnName = columnName;
            this.type = String.class;
        }
        /// <summary>
        /// copy结构
        /// </summary>
        /// <returns></returns>
        public DataColumn Clone()
        {
        	DataColumn column = new DataColumn(this.columnName, this.type);
            return column;
        }
        /// <summary>
        /// copy数据
        /// </summary>
        /// <returns></returns>
        public DataColumn Copy()
        {
        	DataColumn column = new DataColumn(this.columnName, this.type);
            for (Object i : this.data)
            {
                column.data.add(i);
            }
            return column;
        }
        
        public Object setObject(int i,Object value)
        {
        	if (this.data.size()> i)
            {
                this.data.set(i, value);
            }
            else
            {
                this.data.add(value);
            }
			return value;
        }
        public Object getObject(int i) throws Exception
        {
        	Object a=null;
            try
            {
                a = this.data.get(i);
            }
            catch (Exception x)
            {
               throw x;
            }
            return a;	
        }	        
}
