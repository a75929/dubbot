package cn.you.GenghisKhan.db.relation.mybatis;

public enum ExampleEnum {
	selectByExample,
	selectByPrimaryKey,
	selectByExampleWithBLOBs,
	deleteByPrimaryKey,
	deleteByExample,
	insert,
	insertSelective,
	countByExample,
	updateByExample,
	updateByExampleSelective,
	updateByPrimaryKeySelective,
	updateByPrimaryKey,
	updateByExampleWithBLOBs,
	updateByPrimaryKeyWithBLOBs,
	selectLimitedList,
	selectByPage
}
