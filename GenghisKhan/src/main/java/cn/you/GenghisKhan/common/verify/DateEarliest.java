package cn.you.GenghisKhan.common.verify;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;

import static java.lang.annotation.ElementType.*;

/**
 * 最早的时间
 * value=30 参数中的时间不能超过30秒之前
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
public @interface DateEarliest {
    String message() default "";
    long value();

}
