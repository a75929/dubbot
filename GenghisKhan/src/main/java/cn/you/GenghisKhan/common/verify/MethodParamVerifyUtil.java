package cn.you.GenghisKhan.common.verify;

import cn.you.GenghisKhan.common.internal.ex.ConvertException;
import cn.you.GenghisKhan.common.utils.DateUtil;
import cn.you.GenghisKhan.common.utils.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

public class MethodParamVerifyUtil {
    /**
     * 参数合法性校验
     * 1.校验参数类型
     * 2.校验参数注解
     * 3.相关参数校验
     * @param parameter
     * @param object
     * @param bizParams
     * @return
     */
    public static Object Verify(Parameter parameter, Object object,Map<String, Object> bizParams)
    {
        if(object!=null)
        {
            Class<?>classType=parameter.getType();
            object= StringUtils.ConvertToType(classType,object.toString());
        }
        Annotation[] annotations = parameter.getDeclaredAnnotations();
        for (Annotation annotation:annotations) {
            if (annotation.annotationType() == NotEmpty.class)
            {
                if(object==null)
                {
                    throw new ConvertException(((NotEmpty)annotation).message());
                }
            }
            else if(annotation.annotationType()== Min.class)
            {
                Min min=(Min)annotation;
                if((Integer)object<min.value())
                {
                    throw new ConvertException(min.message());
                }
            }
            else if(annotation.annotationType()== Max.class)
            {
                Max max=(Max)annotation;
                if((Integer)object>max.value())
                {
                    throw new ConvertException(max.message());
                }
            }
            else if(annotation.annotationType()== RegionStr.class)
            {
                RegionStr regionStr=(RegionStr)annotation;
                String[] values=regionStr.value();
                if(!Arrays.asList(values).contains(object))
                {
                    throw new ConvertException(regionStr.message());
                }
            }
            else if(annotation.annotationType()== DateEarliest.class)
            {
                DateEarliest dateEarliest=(DateEarliest)annotation;
                long value=dateEarliest.value();
                long intervalTime= DateUtil.calLastedTime((Date)object,new Date());
                if(value<intervalTime)
                {
                    throw new ConvertException(dateEarliest.message());
                }
            }
            else if(annotation.annotationType()== DateLast.class)
            {
                DateLast dateLast=(DateLast)annotation;
                long value=dateLast.value();
                long intervalTime= DateUtil.calLastedTime((Date)object,new Date());
                if(value>intervalTime)
                {
                    throw new ConvertException(dateLast.message());
                }
            }
            else if(annotation.annotationType()== DateGreater.class)
            {
                DateGreater dateGreater=(DateGreater)annotation;
                Object starttime =bizParams.get(dateGreater.value());
                starttime=StringUtils.ConvertToType(Date.class,starttime.toString());
                long c= DateUtil.calLastedTime((Date)starttime,(Date)object);
                if(c<0)
                {
                    throw new ConvertException(dateGreater.message());
                }
            }
            else
            {
                throw new ConvertException("有未处理的业务参数约束");
            }
        }
        return object;
    }

}
