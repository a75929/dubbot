package cn.you.GenghisKhan.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Mtime on 2017/9/5.
 */
public class PropertyUtil {
    private static final Logger logger = LoggerFactory.getLogger(PropertyUtil.class);
    private static Map<String, String> propertiesMap = new HashMap<>();
    // Default as in PropertyPlaceholderConfigurer

    static {
        loadAllProperties();
    }

    public static void processProperties( Properties props) throws BeansException {

        propertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString().trim();
            try {
                //PropertiesLoaderUtils的默认编码是ISO-8859-1,在这里转码一下
                propertiesMap.put(keyStr, new String(props.getProperty(keyStr).getBytes("ISO-8859-1"),"utf-8").trim());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            };
        }
        logger.info(String.valueOf(propertiesMap));
        //System.out.println(propertiesMap);
    }
    public static void loadAllProperties(){
        try {

            Properties properties = PropertiesLoaderUtils.loadAllProperties("application.properties");
            processProperties(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String name) {
        if (null!=propertiesMap.get(name)){
            return propertiesMap.get(name).toString();
        }
        return  null;

    }

    public static void setProperty(String key,String value)
    {
        propertiesMap.put(key,value);
    }
}
