package cn.you.GenghisKhan.common.dataset;


public enum DataType
{
    Null, Bool, Byte, Char, Time, TimeSpan,Date, DateTimeOffset, Decimal,Float, Double, Int16, Int32, Int64, sByte, Single, String, uInt16, uInt32, uInt64, Bytes, Guid, Array, Object
}