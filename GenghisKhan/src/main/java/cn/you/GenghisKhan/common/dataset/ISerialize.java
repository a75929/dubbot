package cn.you.GenghisKhan.common.dataset;

import java.io.IOException;


public interface ISerialize {
	byte[] ToArray () throws IOException,Exception;
    boolean FromBytes (byte[] bytes, RefObject<String> errmsg);
 
}
