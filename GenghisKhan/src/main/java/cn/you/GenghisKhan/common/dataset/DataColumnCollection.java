package cn.you.GenghisKhan.common.dataset;


import java.lang.reflect.Type;
import java.util.ArrayList;
public final class DataColumnCollection extends ArrayList {
	DataTable table;
	public DataColumnCollection(DataTable tableCollection) {
		this.table = tableCollection;
	}
	public DataColumn Add(DataColumn column) {
		column.SetTable(this.table);
		super.add(column);
		return column;
	}
	public DataColumn Add(String columnName, Type type) {
		DataColumn column = new DataColumn(columnName, type);
		return Add(column);
	}	
	public void AddRange(DataColumn[] columns) {
		for(DataColumn col :columns){
			col.SetTable(this.table);
			super.add(col);
		}
	}
	public DataColumn getColumn(String name) throws Exception {
		if (name == null) {
			throw new Exception("错误！字段名不能为null。");
		}
		for (Object c : super.toArray()) {
			DataColumn column = (DataColumn) c;
			if (column.columnName.equalsIgnoreCase(name))
				return column;
		}
		return null;
	}
	
	public boolean Contains(String columnName) {
		// 如果当前集合存在明细为value的明细则返回true
		for (Object c : super.toArray()) {
			DataColumn column = (DataColumn) c;
			if (column.columnName.equals(columnName))
				return true;
		}
		return false;
	}

	public DataColumn getColumn(int i) throws Exception {
		DataColumn column;
		try {
			column = (DataColumn) super.get(i);
		} catch (Exception x) {
			throw new Exception("此索引处不存在列");
		}
		return column;

	}
	
	public int IndexOf(DataColumn value) {
		// 获取指定元素的位置
		return super.indexOf(value);
	}

	public void Remove(DataColumn value) {
		// 移除
		super.remove(value);
	}

	public void Remove(String value) {
		for (Object c : super.toArray()) {
			DataColumn column = (DataColumn) c;
			if (column.columnName.equals(value)) {
				Remove(column);
				break;
			}

		}
	}
}
