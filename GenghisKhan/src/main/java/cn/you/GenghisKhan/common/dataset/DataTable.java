package cn.you.GenghisKhan.common.dataset;
import java.util.ArrayList;
import java.util.List;

 
public class DataTable {
	 DataSet dataSet;
     public DataColumnCollection Columns;
     public DataRowCollection Rows;
     String tableName;
	public DataTable(DataSet operaBytes)
     {
         this.dataSet = operaBytes;
         this.Columns = new DataColumnCollection(this);
         this.Rows = new DataRowCollection(this);
     }
	public DataTable(String name,DataSet operaBytes)
    {
		this.tableName=name;
        this.dataSet = operaBytes;
        this.Columns = new DataColumnCollection(this);
        this.Rows = new DataRowCollection(this);
    }
	
     public DataTable()
     {
//    	 this(null);
    	 this.dataSet=null;
    	 this.Columns = new DataColumnCollection(this);
         this.Rows = new DataRowCollection(this);
     }
     public DataTable(String tablename)
     {
    	 this.tableName=tablename;
    	 this.dataSet=null;
    	 this.Columns = new DataColumnCollection(this);
         this.Rows = new DataRowCollection(this);
//    	 this(null);
     }
     /**
      * 返回所属的DataSet
      * @return
      */
     public DataSet getDataSet()
     {
        return this.dataSet;    	 
     }
     public DataColumnCollection getColumns() {
 		return Columns;
 	}

 	public void setColumns(DataColumnCollection columns) {
 		Columns = columns;
 	}

 	public DataRowCollection getRows() {
 		return Rows;
 	}
 	
 	public DataRow getRow(int index) throws Exception{
 		return Rows.getRow(index);
 	}
 	
 	public void setRows(DataRowCollection rows) {
 		Rows = rows;
 	}
 	/**
 	 * 获取行的指定列名的数据
 	 * */
 	public Object getObject(int rowIndex,String columnname) throws Exception{
 		return Rows.getRow(rowIndex).getObject(columnname);
 	}
 	/**
 	 * 获取行的指定列序号的数据
 	 * */
 	public Object getObject(int rowIndex,int colIndex) throws Exception{
 		return Rows.getRow(rowIndex).getObject(colIndex);
 	}
     /// <summary>
     /// 根据传入字段列表进行数据去重
     /// </summary>
     /// <param name="distinct"></param>
     /// <param name="columnNames"></param>
     /// <returns></returns>
     public DataTable ToTable(Boolean distinct, String[] columnNames) throws Exception
     {
    	 DataTable table = new DataTable();
      
         if (columnNames.length == 0)
         {
             columnNames = new String[this.Columns.size()];
             for (int j = 0; j < columnNames.length; j++)
             {
                 columnNames[j] = this.Columns.getColumn(j).columnName;
             }
         }
 
         int[] numArray = new int[columnNames.length];
         List<Object[]> arraylist =  new ArrayList<Object[]>();
         for (int i = 0; i < columnNames.length; i++)
         {
             DataColumn column = this.Columns.getColumn(columnNames[i]);
             if (column == null)
             {
                 throw new Exception("去重列不在此table中！");
             }
             table.Columns.Add(column.Clone());
             numArray[i] = this.Columns.IndexOf(column);
         }
 
         for (Object c : this.Rows.toArray())
         {
        	 DataRow view=(DataRow)c;
             Object[] objectArray = new Object[columnNames.length];
             for (int k = 0; k < numArray.length; k++)
             {
                 objectArray[k] = view.getObject(numArray[k]);
             }
             if (!distinct || !this.RowExist(arraylist, objectArray))
             {
                 table.Rows.add(objectArray);
                 arraylist.add(objectArray);
             }

         }

         return table;
     }
     /// <summary>
     /// 转换成DataTable
     /// </summary>
     /// <returns></returns>
     public DataTable ToDataTable() throws Exception
     {
         DataTable tb = new DataTable();
         int count = this.Columns.size();
         for (Object c : this.Columns.toArray())
         {
            DataColumn column =(DataColumn)c;
            tb.Columns.Add(column);
         }
         for (Object r : this.Rows.toArray())
         {
        	 DataRow row=(DataRow)r;
             Object[] ob = new Object[count];
             for (int i = 0; i < count; i++)
             {
                 ob[i] = row.getObject(i);
             }
             tb.Rows.Add(ob);
         }
         return tb;

     }
     private boolean RowExist(List<Object[]> arraylist, Object[] objectArray)
     {
         for (int i = 0; i < arraylist.size(); i++)
         {
        	 Object[] objArray = arraylist.get(i);
        	 boolean flag = true;
             for (int j = 0; j < objectArray.length; j++)
             {
                 flag &= objArray[j].equals(objectArray[j]);
             }
             if (flag)
             {
                 return true;
             }
         }
         return false;
     }

     protected  DataTable CreateInstance()
     {
    	 return null;
         //return (DataTable)Activator.CreateInstance(this.getClass(), true);
     }
     //克隆表结构
     public DataTable Clone()
     {
    	 DataTable initDataTable = new DataTable();
    	 initDataTable.setTableName(this.tableName);
    	 for (Object c : this.Columns.toArray()) {
 			DataColumn column = (DataColumn) c;
 			initDataTable.Columns.Add(column.columnName,column.type);
 		}
 	 
    	 return initDataTable;
     }
     /// <summary>
     /// 表数据复制
     /// </summary>
     /// <returns></returns>
     public DataTable Copy() throws Exception
     {
    	 DataTable table = this.Clone();
         int count = this.Columns.size();
//         for (int i = 0; i < count; i++)
//         {
//             DataColumn column = this.Columns.getColumn(i);
//             table.Columns.Add(column.Clone());
//         }
         for (Object c : this.Rows.toArray())
         {
        	 DataRow view=(DataRow)c;
        
             Object[] objectArray = new Object[count];
             for (int k = 0; k < count; k++)
             {
                 objectArray[k] = view.getObject(k);
             }
             table.Rows.Add(objectArray);
         }
         return table;
     }

     /// <summary>
     /// Table比较，比较提交必须列和类型全部相同
     /// </summary>
     /// <param name="mydt"></param>
     /// <returns></returns>
     public Boolean Compare(DataTable mydt) throws Exception
     {
         if (this.Columns.size() != mydt.Columns.size())
             return false;
         for (int i = 0; i < this.Columns.size(); i++)
         {
             if (mydt.Columns.Contains(this.Columns.getColumn(i).columnName) == false)
             {
                 return false;
             }
             else if (this.Columns.getColumn(i).type != mydt.Columns.getColumn(this.Columns.getColumn(i).columnName).type)
             {
                 return false;
             }
         }
         return true;
     }

     /// <summary>
     /// 数据合并
     /// </summary>
     /// <param name="mydt">数据合并</param>
     /// <returns></returns>
     public DataTable Merge(DataTable mydt) throws Exception
     {
         if (Compare(mydt))
         {
             int count = mydt.Rows.size();
             int columnCount = mydt.Columns.size();
             if (count>0)
             {
                 for (Object c : mydt.Rows.toArray())
                 {
                	 DataRow view=(DataRow)c;
         
                	 Object[] objectArray = new Object[columnCount];
                     for (int k = 0; k < columnCount; k++)
                     {
                         objectArray[k] = view.getObject(k);
                     }
                     this.Rows.Add(objectArray);
                 }
             }
         }
         return this;
     }
     public String getTableName() {
 		return tableName;
 	}
 	public void setTableName(String tableName) {
 		this.tableName = tableName;
 	}
 	/**
 	 * 查找满足条件的第一个行
 	 * @param columnName
 	 * @param value
 	 * @return
 	 * @throws Exception
 	 */
 	public DataRow findFirstRow(String columnName,String value) throws Exception
 	{
 		int rows=this.Rows.size();
 		String rvalue;
 		for (int i=0;i<rows;i++)
 		{ 
			rvalue =this.getObject(i, columnName).toString(); 
 			if(value.equalsIgnoreCase(rvalue))
 			{
 			  return this.Rows.getRow(i);
 			}
 		}
 		return null; 
 	}
 	/**
 	 * 查找满足条件的第一个行的数据值
 	 * @param columnName
 	 * @param value
 	 * @param pcolumnName
 	 * @return
 	 * @throws Exception
 	 */
    public Object findFirstRowValue(String columnName,String value,String pcolumnName) throws Exception 
    {
    	DataRow finddr=findFirstRow(columnName,value);
    	if(finddr==null)
    	{
    		return null; 
    	}
        return 	finddr.getObject(pcolumnName);
    }

}
